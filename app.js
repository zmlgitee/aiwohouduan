var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mysql=require('mysql')

var indexRouter = require('./routes/index.js');
var userRouter = require('./routes/user.js');
const detRouter=require('./routes/details.js');
const caseRouter=require('./routes/case.js');
const hotelRouter=require('./routes/hotel.js');
const vehicleRouter=require('./routes/vehicle.js')
var app = express();



// 加载CORS模块
const cors = require('cors');
// 使用CORS中间件
app.use(cors({
  origin: ['http://localhost:8080', 'http://127.0.0.1:8080']
}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/index', indexRouter);
app.use('/users', userRouter);
app.use('/case',caseRouter);
app.use('/wedding',detRouter);
app.use('/hotel',hotelRouter);
app.use('/vehicle',vehicleRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
