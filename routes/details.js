//引入express模块
const express=require('express');
//引入连接池模块
const pool=require('../pool.js');
//console.log(pool);
//创建路由器对象
const d = express.Router();
//往路由器中添加路由
//接口的地址：http://127.0.0.1:3000/wedding
//请求的方法：get
 d.get('/details/:wid',(req,res)=>{
     let wid = req.params.wid;
     pool.query(`select * from wedding_case where wid=${wid}`,(err,result)=>{
         if(err){
              throw err;
         }
         console.log(result);
         res.send({code:200,msg:'ok',data:result});
     });
 });
d.get('/detailspic/:wid',(req,res)=>{
    let wid = req.params.wid;
    pool.query(`select * from wedding_case_pic where wid=${wid}`,(err,result)=>{
        if(err){
            throw err;
       }
       console.log(result);
       res.send({code:200,msg:'ok',data:result});
    })
})
d.get('/detailsteam/:wid',(req,res)=>{
    let wid = req.params.wid;
    pool.query(`select * from detailsteam where wid=${wid}`,(err,result)=>{
        if(err){
            throw err;
       }
       console.log(result);
       res.send({code:200,msg:'ok',data:result});
    })
})
//导出路由器对象
module.exports=d;