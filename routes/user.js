//引入express模块
const express=require('express');
//引入连接池模块
const pool=require('../pool.js');
//console.log(pool);
//创建路由器对象
const r=express.Router();
//往路由器中添加路由
//1.用户注册接口(post /reg)
//接口的地址：http://127.0.0.1:3000/users/reg
//请求的方法：post
r.post('/reg',(req,res,next)=>{
  //1.1获取post请求的数据
  let obj=req.body;
  console.log(obj);
  //1.3执行SQL命令
  pool.query('insert into user set ?',[obj],(err,result)=>{
    if(err){
	  //如果出现错误交给下一个中间件来处理
      next(err);
	  //阻止往后执行
      return;
	}
	console.log(result);
    res.send({code:200,msg:'注册成功'});
  });
});
//2.检查手机号是否存在
//接口的地址:http://127.0.0.1:3000/users/phone
//请求的方法：post
r.post('/phone',(req,res,next)=>{
  let obj=req.body;
  pool.query('select * from user where phone=?',[obj.phone],(err,result)=>{
    if(err){
      next(err);
      return;
    }
    console.log(result);
    if(result.length==0){
      res.send({code:200,msg:"验证通过"})
    }else{
      res.send({code:201,msg:"用户已存在"})
    }
  })
})
//3.用户登录的接口(post /login)
//接口的地址：http://127.0.0.1:3000/users/login
//请求的方法：post
r.post('/login',(req,res,next)=>{
  //2.1获取post请求的数据
  let obj=req.body;
  //2.3执行SQL命令，到数据库中查询有没有用户名和密码匹配的数据
  pool.query('select * from user where phone=? and pwd=?',[obj.phone,obj.pwd],(err,result)=>{
    if(err){
	  //有错误把它交个下一个错误处理中间件
	  next(err);
	  return;
	}
	//结果是数组，如果是空数组，说明登录失败，否则登录成功
	if(result.length===0){
	  res.send({code:201,msg:'登录失败'});
	}else{
	  res.send({code:200,msg:'登录成功',data:result});
	}
  });
  
});
//4.用户信息(get /information)
//接口的地址：http://127.0.0.1:3000/users/information
//请求的方法：post
r.post('/information',(req,res)=>{
  pool.query('select * from user where phone=?',[req.body.phone],(err,result)=>{
    if(err)throw err;
    if(result.length==0){
      res.send({code:201,msg:'查找失败'})
    }else{
      res.send({code:200,msg:'查找成功',data:result})
    }
  })
})
//4.用户信息修改(post /modify_information)
//接口的地址：http://127.0.0.1:3000/users/modify_information
//请求的方法：post
r.post('/modify_information',(req,res)=>{
  console.log(req.body);
  pool.query('update  user  set  uname=?,address=?,sex=? where  phone=?;',[req.body.uname,req.body.address,req.body.sex,req.body.phone],(err,result)=>{
    if(err)throw err;
    res.send({code:200,msg:'修改成功'})
  })
})

//4.密码修改(post /modify_pwd)
//接口的地址：http://127.0.0.1:3000/users/modify_pwd
//请求的方法：post
r.post('/modify_pwd',(req,res)=>{
  console.log(req.body);
  pool.query('update  user  set  pwd=? where  phone=? && pwd=?;',[req.body.newpwd,req.body.phone,req.body.pwd],(err,result)=>{
    if(err)throw err;
    if(result.changedRows==1){
    res.send({code:200,msg:'修改成功'})
  }else{
    res.send({code:201,msg:`修改失败`})
  }
  })
})
//导出路由器对象
module.exports=r;