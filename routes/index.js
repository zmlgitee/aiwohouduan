//引入express模块
const express=require('express');
//引入连接池模块
const pool=require('../pool.js');
//console.log(pool);
//创建路由器对象
const i=express.Router();
//首页轮播图
//接口地址:http://127.0.0.1:3000/index/rotation
//请求方法：get
i.get('/rotation',(req,res)=>{
  pool.query('select * from carousel',(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'查询成功',data:result
    })
  })
})

//首页案例列表
//接口地址:http://127.0.0.1:3000/index/case
//请求方法：get
i.get('/case',(req,res)=>{
  pool.query('select * from ucase',(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'查询成功',data:result
    })
  })
})

//首页评论列表
//接口地址:http://127.0.0.1:3000/index/comment
//请求方法：get
i.get('/comment',(req,res)=>{
  pool.query('select * from comment',(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'查询成功',data:result
    })
  })
})

//首页评论列表
//接口地址:http://127.0.0.1:3000/index/hotel
//请求方法：get
i.get('/hotel',(req,res)=>{
  pool.query('select * from hotels',(err,result)=>{
    if(err)throw err;
    res.send({
      code:200,msg:'查询成功',data:result
    })
  })
})
module.exports=i;