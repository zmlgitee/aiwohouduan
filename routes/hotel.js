//引入express模块
const express=require('express');
//引入连接池模块
const pool=require('../pool.js');
//console.log(pool);
//创建路由器对象
const hotel=express.Router();
//往路由器中添加路由
//1.员工查询接口(post /reg)
//接口的地址：http://127.0.0.1:3000/hotel/hotel
//请求的方法：get
hotel.get('/hotel',(req,res,next)=>{
  //查询的sql
  pool.query('select * from hotel',(err,data)=>{
    //处理错误
    if(err){next(err);return;}
    //查看数据库返回的数据data
    // console.log(data);
    res.send({
      "code":1,
      "msg":"已查到所有数据",
      "data":data
    })
  })
})
hotel.get('/details/:id',(req,res,next)=>{
  pool.query(`select * from hotel_details where hid=${req.params.id}`,(err,data)=>{
    if(err){next(err);return}
    res.send({
      code:1,
      msg:"查询成功",
      data:data
    })
  })
})

//导出路由器对象
module.exports=hotel;