const express=require('express');
//引入连接池模块
const pool=require('../pool.js');
//console.log(pool);
//创建路由器对象
const v = express.Router();
//往路由器中添加路由
//接口的地址：http://127.0.0.1:3000/weddingcase
//请求的方法：get
v.get('/car',(req,res)=>{
    pool.query('select * from vehicle',(err,result)=>{
        if(err){
             throw err;
        }
        console.log(result);
        res.send({code:200,msg:'ok',data:result});
    });
});

module.exports=v;