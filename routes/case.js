//引入express模块
const express=require('express');
//引入连接池模块
const pool=require('../pool.js');
//console.log(pool);
//创建路由器对象
const c = express.Router();
//往路由器中添加路由
//接口的地址：http://127.0.0.1:3000/weddingcase
//请求的方法：get
 c.get('/weddingcase/:page',(req,res)=>{
     let page = Number(req.params.page);

     pool.query('select * from wedding_case limit ?,20',[(page-1)*20],(err,result)=>{
         if(err){
              throw err;
         }
         console.log(result);
         res.send({code:200,msg:'ok',data:result});
     });
 });
 c.get('/weddingcase',(req,res)=>{
    pool.query('select * from wedding_case',(err,result)=>{
        if(err){
             throw err;
        }
        console.log(result);
        res.send({code:200,msg:'ok',data:result});
    });
});
//导出路由器对象
module.exports=c;